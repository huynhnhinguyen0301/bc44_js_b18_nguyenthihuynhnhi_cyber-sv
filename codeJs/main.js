var getNameFromTr = function (trTag) {
  var tdList = trTag.querySelectorAll("td");
  //vị trí của thẻ td trong "mảng tr" là thứ 2
  return tdList[2].innerText;
};

var getScoreFromTr = function (trTag) {
  var tdList = trTag.querySelectorAll("td");
  //vị trí của thẻ td trong "mảng tr" là thứ 3
  return tdList[3].innerText * 1;
};

//Lấy thẻ td chứa điểm
var tdList = document.querySelectorAll(".td-scores");
var scoreList = [];
// tạo array chứa điểm của các thẻ td
for (let index = 0; index < tdList.length; index++) {
  // duyệt mảng trong biến đã lấy được giá trị và push vào 1 mảng mới
  var score = tdList[index].innerHTML * 1;
  scoreList.push(score);
}

var max = scoreList[0];
for (let index = 0; index < scoreList.length; index++) {
  if (scoreList[index] > max) {
    max = scoreList[index];
  }
}
var min = scoreList[0];
for (let index = 0; index < scoreList.length; index++) {
  if (scoreList[index] < min) {
    min = scoreList[index];
  }
}

//danh sách thẻ tr
var trList = document.querySelectorAll("#tblBody tr");

//yc1: in ra sinh viên giỏi nhất
//vị trí của số lớn nhất
var indexMax = scoreList.indexOf(max);
//vị trí index nhỏ nhất
var indexMin = scoreList.indexOf(min);

function showSinhVienGioiNhat() {
  var name = getNameFromTr(trList[indexMax]);
  var score = getScoreFromTr(trList[indexMax]);
  document.getElementById("svGioiNhat").innerText = `${name} - ${score}`;
}
showSinhVienGioiNhat();

function showSinhVienYeuNhat() {
  var name = getNameFromTr(trList[indexMin]);
  var score = getScoreFromTr(trList[indexMin]);
  document.getElementById("svYeuNhat").innerText = `${name} - ${score}`;
}
showSinhVienYeuNhat();

//yêu cầu 3: đếm số sinh viên giỏi
// tạo 1 mảng gồm những số lớn hơn 8

function demSoSinhVienGioi() {
  var resultList = scoreList.filter(function (score) {
    return score >= 8;
  });
  document.getElementById("soSVGioi").innerText = resultList.length;
}
demSoSinhVienGioi();

//danh sách sinh viên điểm dưới 5
//IIFE : NẾU MUỐN VIẾT VÀ CHẠY LIỀN FUNCTION : (function Name(Params){})();


//yêu cầu 4: in danh sách sinh viên điểm trung bình lớn hơn 5
function inDanhSachSVDTBLonHon5() {
  var contentResult = "";
  for (let index = 0; index < trList.length; index++) {
    var currentTr = trList[index];
    var score = getScoreFromTr(currentTr);
    if (score > 5) {
      //nếu điểm thẻ tr hiện tại lớn hơn 5 thì tiếp tục lấy tên và lưu lại
      var name = getNameFromTr(currentTr);
      var content = `<p>${name} - ${score}</p>`;
      contentResult += content;
    }
  }
  document.getElementById("dsDiemHon5").innerHTML = contentResult;
}
inDanhSachSVDTBLonHon5();